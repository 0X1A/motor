#include "filesystem.h"

Filesystem::Filesystem()
{
}

Filesystem::~Filesystem()
{
}

// Sets which archive for PhysFS to open
// @Archive archive to be opened
int Filesystem::SetArchive(const char *Archive)
{
	PHYSFS_mount(Archive, NULL, 1);

	if (PHYSFS_mount(Archive, NULL, 1) != 0) {
		printf("Archive %s added/exist\n", Archive);
		return 1;
	} else if (PHYSFS_mount(Archive, NULL, 1) == 0) {
		printf("Archive: %s not added/doesn't exist\nPhysFS Error: %s\n", Archive, PHYSFS_getLastError());
		return -1;
	}
	return 0;
}

// Checks for file existance in archive
// @FChck file to be checked
int Filesystem::FileCheck(const char *FChck)
{
	PHYSFS_exists(FChck);

	if (PHYSFS_exists(FChck) != 0) {
		printf("File: %s exists in archive\n", FChck);
		return 1;
	} else if (PHYSFS_exists(FChck) == 0) {
		printf("File: %s doesn't exist\nPhysFS Error: %s\n", FChck, PHYSFS_getLastError());
		return -1;
	}
	return 0;
}

// Opens file in archive
// @PhysFile file to be opened
const char *Filesystem::OpenFile(const char *PhysFile)
{
	if (PHYSFS_openRead(PhysFile) == NULL) {
		std::cout << "File " << PhysFile << " cannot be opened\n";
		return NULL;
	} else if (PHYSFS_openRead(PhysFile) != NULL) {
		printf("File %s opened\n", PhysFile);
		FS_File = PHYSFS_openRead(PhysFile);
		Size = PHYSFS_fileLength(FS_File);
		Data = new char[PHYSFS_fileLength(FS_File)];
		PHYSFS_read(FS_File, Data, 1, Size);
		return PhysFile;
	}
	return NULL;
}

int Filesystem::CloseFile()
{
	if (FS_File != NULL) {
		delete[] Data;
		return 1;
		Data = NULL;
	}
	return 0;
}
