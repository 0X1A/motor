#include "game.h"

Game::Game()
{
}

int Game::Config(const std::string GameName, int Width, int Height, const std::string Icon)
{
	CreateWindow(Width, Height, GameName);
	SetWindowIcon(Icon);
	return 0;
}

Game::~Game()
{
	DestRenderer();
	DestWindow();
}
