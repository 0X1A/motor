#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include <physfs.h>
#include <fstream>
#include <iostream>
#include <string>

class Filesystem
{
public:
	const char *Archive;
	const char *Dir;
	const char *FChck;
	char *Data;
	int Priority;
	PHYSFS_sint64 Size;
	PHYSFS_file *FS_File;

public:
	Filesystem();
	~Filesystem();
	int SetArchive(const char *Archive);
	int FileCheck(const char *FChck);
	int CloseFile();
	const char *OpenFile(const char *PhysFile);
};

#endif
